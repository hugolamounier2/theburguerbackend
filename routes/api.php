<?php

use App\Http\Controllers\OrderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/csrf_token', function () {
    return response()->json(csrf_token());
});

Route::get('/order', [OrderController::class, 'getAll'])->name('order.get');
Route::get('/order/{order}', [OrderController::class, 'get'])->name('order.get');
Route::post('/order', [OrderController::class, 'create'])->name('order.create');
Route::put('/order', [OrderController::class, 'update'])->name('order.update');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
