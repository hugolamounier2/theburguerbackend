<?php

namespace App\Http\Controllers;

use App\Enums\OrderType;
use App\Models\Dtos\OrderData;
use App\Models\Dtos\ProductData;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use Exception;
use Illuminate\Support\Facades\Config;

use const Cerbero\Dto\CAMEL_CASE_ARRAY;

class OrderController extends Controller
{
    public function getAll()
    {
        try {
            $orders = Order::with("products")->get()->toBase();

            $returnObj = [];

            foreach($orders as $order)
            {
                array_push($returnObj, OrderData::fromModel($order, CAMEL_CASE_ARRAY));
            }

            sleep(2);

            return response()->json($returnObj, 200);
        } catch (Exception $e) {
            return response()->json($e->getMessage(), 400);
        }
    }

    public function get($id)
    {
        try {
            $order = Order::with("products")->find($id);

            $orderDto = OrderData::fromModel($order, CAMEL_CASE_ARRAY);

            return response()->json($orderDto, 200);
        } catch (Exception $e) {
            return response()->json($e->getMessage(), 400);
        }
    }

    public function create()
    {
        try {
            $Order = new Order();
            $Order->status = OrderType::OrderStatus['WAITING_CONFIRMATION'];
            $Order->save();

            return response()->json([
                "id" => $Order->id,
                "createdAt" => $Order->created_at,
            ], 200);
        } catch (Exception $e) {
            return response()->json($e->getMessage(), 400);
        }
    }

    public function update(Request $request)
    {
        try {
            $orderDto = OrderData::fromRequest($request);

            $order = Order::with("products")->find($orderDto->id);

            $order->customer_id = $orderDto->customerId;
            $order->details = $orderDto->details;
            $order->discount = $orderDto->discount;

            $order->products()->delete();

            foreach ($orderDto->products as $product) {
                $addProduct = new Product();
                $addProduct->name = $product->name;
                $addProduct->price = $product->price;

                $order->products()->save($addProduct);
            }

            $order->save();

            return;
        } catch (Exception $e) {
            return response()->json($e->getMessage(), 400);
        }
    }
}
