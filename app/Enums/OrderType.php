<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class OrderType extends Enum
{
    const OrderStatus = [
        'WAITING_CONFIRMATION' => 1,
        'IN_PRODUCTION' => 2,
        'IN_DELIVERY' => 3,
        'DONE' => 4
    ];
}
