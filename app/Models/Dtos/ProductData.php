<?php

namespace App\Models\Dtos;

use Carbon\Carbon;
use Cerbero\LaravelDto\Dto;

use const Cerbero\Dto\PARTIAL;
use const Cerbero\Dto\IGNORE_UNKNOWN_PROPERTIES;

/**
 * The data transfer object for the Product model.
 *
 * @property Carbon|null $createdAt
 * @property int $id
 * @property string $name
 * @property float $price
 * @property Carbon|null $updatedAt
 */
class ProductData extends Dto
{
    /**
     * The default flags.
     *
     * @var int
     */
    protected static $defaultFlags = PARTIAL | IGNORE_UNKNOWN_PROPERTIES;
}
