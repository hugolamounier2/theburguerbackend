<?php

namespace App\Models\Dtos;

use Carbon\Carbon;
use Cerbero\LaravelDto\Dto;

use const Cerbero\Dto\PARTIAL;
use const Cerbero\Dto\IGNORE_UNKNOWN_PROPERTIES;

/**
 * The data transfer object for the Order model.
 *
 * @property Carbon|null $createdAt
 * @property int|null $customerId
 * @property string|null $details
 * @property float|null $discount
 * @property int $id
 * @property int $status
 * @property Carbon|null $updatedAt
 * @property ProductData[] $products
 */
class OrderData extends Dto
{
    /**
     * The default flags.
     *
     * @var int
     */
    protected static $defaultFlags = PARTIAL | IGNORE_UNKNOWN_PROPERTIES;
}
